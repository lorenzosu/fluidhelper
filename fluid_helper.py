#!/usr/bin/env python3
""" A helper to launch multiple soundfonts with fluidsynth and xterm """
import logging
import sys
import os

from fluidhelper import configreader
from fluidhelper import launcher
from fluidhelper import zenitygui
from fluidhelper import argparser
from fluidhelper import filehandlers
from fluidhelper import jackutils

def logging_setup(lev=logging.WARNING):
    """ Set-up logging """
    logger = logging.getLogger('fluidhelper')
    logger.setLevel(lev)

    console_hhandler = logging.StreamHandler()
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    console_hhandler.setFormatter(formatter)
    logger.addHandler(console_hhandler)
    return logger

class FluidHelper:
    """
    Get arguments, if needed launch zenity dialog, then start xterm + fluidsynth

    At the end when all xterm/fluidsynth windows are closed we propose to save
    the soundfont list to a file (e.g. usable as input of the programme), but
    we do it only if the file were selected via the command line or zenity as it
    doesn't make sense if they were already loaded from a file. Thus the
    save_prompt variable
    """
    def __init__(self):
        """ Init with log and args and save_prompt flag """
        self.log = logging_setup(logging.INFO)
        self.args = argparser.argparser()
        self.save_prompt = False

        if not jackutils.is_jack_or_pipewire_running():
            self.log.error(
                "Can't find pid for a required JACK."
                "Is JACK running?"
                "\nAborting ")
            sys.exit(2)


        self.file_list = []
        # we do this in case that the script is run e.g. as a symlink
        config_path = self.get_config_path()
        self.flag_list = configreader.configreader(config_path)

    def get_config_path(self, filename='fluidhelprc'):
        """ Get the configuration path for the config file 'fluidhelprc' """
        # TODO do not hardcode filename
        path = os.path.realpath(__file__)
        conf_path = os.path.join(
            os.path.dirname(path),
            filename
        )
        return conf_path

    def run(self):
        """ run function """
        # Three options:
        # 1. files specified as command line argument
        if self.args.files is not None:
            self.file_list = self.args.files
            self.save_prompt = False
        # 2. files saved in a plain-text file specified as command-line argument
        elif self.args.load is not None:
            self.file_list = filehandlers.read_from_file(self.args.load)
            self.save_prompt = False
        # 3. (default) launch a zenity gui to select the files
        else:
            self.file_list = zenitygui.zenity_select_files()
            self.save_prompt = True

        # if for any reason the file list is empty (e.g. user cancelled dialog)
        # we return
        if self.file_list == []:
            self.log.info("SF2 file list is empty. Can't continue")
            return 1

        self.log.info("Launching xterm with fluidsynth...")
        # TODO: check if JACK is running
        fsynth_launch = launcher.FluidSynthLauncher(self.file_list, self.flag_list)
        fsynth_launch.launch()
        if not self.save_prompt:
            print("Bye!")
            sys.exit(0)
        else:
            self.ask_for_save()
        print("Bye!")

    def ask_for_save(self):
        """ At the end prompt for saving sf2 list and if yes save to file"""
        # The file_list might be empty if the user cancelled selection...
        if self.file_list == []:
            return
        save_list_to_file = False
        save_list_to_file = zenitygui.zenity_question(
            'Save soundfont list to file?\n'
            '(if yes a save dialog will be opened)')
        if save_list_to_file:
            save_path = zenitygui.zenity_save_file_selector()
            filehandlers.save_filelist_to_file(save_path, self.file_list)


if __name__ == "__main__":
    f_h = FluidHelper()
    f_h.run()
