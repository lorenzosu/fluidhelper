# What
A helper program to launch multiple FluidSynth instances for multiple SoundFonts.

The program actually launches an xterm which in turn runs fluidsynth with a set of configurable flags and the right soundfont. It assumes JACK and alsa midi are used.
See the [screengrab](https://gitlab.com/lorenzosu/fluidhelper/-/raw/main/screengrab.gif) below to get an idea:

![image](./screengrab.gif)

# Why
While more advanced programs like Qsynth (https://qsynth.sourceforge.io/) exist for loading multiple soundfonts, I needed a program which would let me create soundfont 'sets' to possibly re-load, additionally being able to interact with fluidsynth via its command line for each loaded soundfont (thus the use of xterm).

# Requirements
- Python 3
- fluidsynth (https://www.fluidsynth.org/)
- xterm (or equivalent terminal with option to run a command with -e)
- JACK (https://jackaudio.org/)
- zenity (https://gitlab.gnome.org/GNOME/zenity)

# How
Once all requirements are satisfied (see below) and JACK is running, just run:

```./fluid_helper.py```

Then select the soundfonds you want to launch with fluidsynth. When done answer 'no' to the prompt and a number of xterm instances running respective fluidsynth ones will be launched. You can interact with each fluidsynth (see fluidsynth help).
If needed you can close any instance with CTRL+C or typing 'quit' at the fluidsynth prompt (after focusing it). After all instances are closed you are prompted to save the soundfont file list to a file for later re-use.

FluidHelper has 3 modes (depending on the command line arguments):
1. a list of sf2 files (full path) are specified on the command line via the `--files` flag. They have to be space-separated absolute paths
2. the list of sf2 files to launch are read from a file via the `--load` flag. The file must contain a sf2 file per line (full paths)
3. if no command line argument is passed a simple zenity-based gui is started allowing to select sounfont files. In this case after closing all the fluidsynth instances the user is also promped to save the used sf2 files to a file (formatted to be used later with the --load flag).

# Configuration
Fluidsynth is always launched with some default options. To modify these check the `fluidhelprc` file. The options are exactly as used in fluidsynth so these will all be added via the -o flag when launching fluidsynth: run `fluidsynth -o help` to see all possible options.

# TODO
- Check if JACK is running (possibly offer to if not)
- Option to dynamically set configuration file (possibly specify it in saved file)
- Additional error-checking
- Integrated GUI?
- Support for other terminals (in config or commandline, or both)
- Investigate using fluidsynth in server mode - but we do need to be able to interact with each soundfont individually
