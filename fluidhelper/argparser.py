""" parse the commandline arguments """

import argparse

__version__ = 'FluidHelper 0.1'

def argparser():
    """ Parse the command line arguments and return them """
    desc = (
        'Launch multiple fluidsynth instances for multiple soundfonts.'
        ' By default if no argument is provided the program will start an'
        ' interactive GUI (using zenity) to selct the files. And eventually'
        ' offer to save the file-list to a text file.'
    )
    parser = argparse.ArgumentParser(description=desc)
    arg_group = parser.add_mutually_exclusive_group()
    arg_group.add_argument(
        '--files',
        type=str,
        nargs='+',
        required=False,
        help='sf2 file(s) separated by a space - absolute path required.'
    )
    arg_group.add_argument(
        '--load',
        type=str,
        required=False,
        help=('load the sounfont list from a file. The file must be formatted'
              ' with 1 full soundfont path per line.')
    )
    parser.add_argument(
        '--version',
        action='version',
        version=__version__)
    args = parser.parse_args()
    return args
