""" Utilities related to JACK which is required """
import subprocess
import logging

module_log = logging.getLogger('fluidhelper.jackutils')

def is_jack_or_pipewire_running():
    """ Check if jack or pipewire are running by searching for their pid.
    Assumes the command pidof is installed (should be on most Linux systems)"""
    
    jack_process_names = ['jackd', 'jackdbus', 'pipewire']
    
    logging.debug('Checking if JACK or PipeWiere are running')
    pid = None
    for jpn in jack_process_names:
        try:
            pidof_proc = subprocess.run(
                ["pidof", jpn, "/dev/null"],
                capture_output=True,
                check=True
            )
            # Assume if at least one PID found then one is running
            if pidof_proc != "":
                return True
        # if no pid is found pidof returns non-zero, therefore this is raised
        except subprocess.CalledProcessError:
            logging.debug('Failed to find pid for %s', jpn)

    return False
