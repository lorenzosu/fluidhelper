""" read and set-up config """
import configparser

def configreader(filename='fluidhelprc'):
    """ Parse the configuration in filename """
    flag_list = []
    config = configparser.ConfigParser()
    config.read(filename)
    fluid_config = config['FluidSynth Commands']
    for x in fluid_config.keys():
        flag_list.extend(["-o", f"{x}={fluid_config[x]}"])
    return flag_list

if __name__ == "__main__":
    f = configreader()
    print(f)
