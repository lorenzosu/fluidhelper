"""
Handle external files (except configuration which has its module)
Read and write files etc.
"""
import logging

module_log = logging.getLogger('filidhelper.filereader')

def read_from_file(file_path):
    """ Read the list from file in file_path """
    with open(file_path, 'r', encoding='utf-8') as f:
        file_lines = []
        for i, line in enumerate(f):
            if line =="\n":
                module_log.warning('Found empty new-line at line %i', i)
            else:
                file_lines.append(line.rstrip())
    return file_lines

def save_filelist_to_file(file_path, file_list):
    """ Save each file in file_list to a file in file_path, one per line """
    with open(file_path, 'w', encoding='utf-8') as f:
        for sf_file in file_list:
            f.write(f"{sf_file}\n")
