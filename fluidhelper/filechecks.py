""" Various checks that executables and files exist """
import os
import sys
import logging

module_logger = logging.getLogger('fluidhelper.filechecks')

def file_exists_or_abort(file_path):
    """ Check if file_path exists or abort with sys.exit. Used in those cases
    when non-existence of a file will compromise execution
    """
    if not os.path.isfile(file_path):
        module_logger.error("%s does not exist. Aborting", file_path)
        sys.exit(2)

def check_command(executable_dict):
    """ Check if needed executables exist, else abort. executable is a
    dictionary in the form {'name': '/path/to/command' e.g.
    {'xterm': '/usr/bin/xterm'} """
    for name, the_exec in executable_dict.items():
        if the_exec is None:
            module_logger.error(
                "Required %s executable not found. Aborting.",
                name
                )
            sys.exit(1)
