""" Quick zenity-based GUI to import files and user interaction """
import subprocess
import shlex
import shutil
import logging

module_log = logging.getLogger('fluidhelper.zenitygui')

def zenity_save_file_selector():
    """ Select a file for saving with zenity """
    command="zenity --file-selection --save --confirm-overwrite"
    split_command = shlex.split(command)
    result = subprocess.run(split_command, capture_output=True, check=True)
    output_string = result.stdout.decode('utf-8')
    output_string = output_string.replace("\n", "")
    return output_string

def run_zenity_file_selection(file_filter="*.sf2"):
    """ Run zenity file selection """
    command = f"zenity --file-selection --multiple --file-filter={file_filter}"
    split_command = shlex.split(command)
    result = subprocess.run(split_command, capture_output=True, check=True)
    output_string = result.stdout.decode('utf-8')
    output_string = output_string.replace("\n", "")
    out_list = output_string.split("|")
    return out_list

def zenity_question(text):
    """ Run a zenity question dialog """
    command = f"zenity --question --text=\"{text}\""
    split_command = shlex.split(command)
    confirm = False
    # subprocess.run raises the exceeption if the command return is non-zero
    try:
        result = subprocess.run(split_command, capture_output=True, check=True)
        confirm = result.returncode == 0
    except subprocess.CalledProcessError:
        confirm = False
    return confirm

def zenity_select_files():
    """ Use zenity to launche open dialog(s) and select soundfonts """
    logging.debug('launching zenity to select files')

    zenity_executable = shutil.which('zenity')
    if zenity_executable is None:
        logging.error(
            """Cannot find zenity executable. This is required at this stage for GUI file selection.
            Most linux distros should include it.
            See here: https://help.gnome.org/users/zenity/stable/
            e.g. Arch: sudo pacman -S zenity
            Ubunto: apt-get install zenity""")
    file_list = []
    do_it = True
    while do_it:
        try:
            result_files = run_zenity_file_selection()
            file_list.extend(result_files)
        # subprocess.run raises exceeption if user clicks Cancel (non-zero ret)
        except subprocess.CalledProcessError:
            logging.warning('User cancelled file selection')
            result_files = []

        do_it = zenity_question("Add other files?")

    return file_list

if __name__ == "__main__":
    zenity_select_files()
