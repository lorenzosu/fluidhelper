""" Module providing the launcher class """
import subprocess
import shutil
import shlex
import os
import time
import logging

from . import filechecks

class FluidSynthLauncher:
    """ Class for the xterm and fluidsynth command launchers """
    def __init__(self, sound_font_list, flag_list):
        self.log = logging.getLogger('fluidhelper.launcher.FluidSynthLauncher')
        self.sound_font_list = sound_font_list
        for f in self.sound_font_list:
            filechecks.file_exists_or_abort(f)

        self.flag_list = flag_list
        self.log.debug("files to launch: %s", self.sound_font_list)
        self.log.debug("fluidsynth common flags: %s", self.flag_list)

        self.executables = {
            'fluidsynth': None,
            'xterm': None
        }
        self.executables['fluidsynth'] = shutil.which('fluidsynth')
        self.executables['xterm'] = shutil.which('xterm')
        filechecks.check_command(self.executables)

    def make_command(self, sf_file, term_offset="+0+0"):
        """ construct the command """
        args = [self.executables['xterm']]
        args.append(f"-geometry")
        args.append(f"80x30{term_offset}")
        args.append("-title")
        args.append(os.path.basename(sf_file))
        args.append("-e")
        flag_string = ' '.join(self.flag_list)
        file_flags = self.make_file_flags(sf_file)
        # surround file path with " for file names/paths with spaces
        sf_file_escape = f'"{sf_file}"'
        fluid_command = ' '.join(
            [
                self.executables['fluidsynth'],
                flag_string,
                file_flags,
                sf_file_escape,
                '2> /dev/null'
            ]
        )
        args.append(fluid_command)
        return shlex.join(args)

    def make_file_flags(self, sf_file, prefix='FluidHelper:'):
        """ Add file-specific flags """
        flag_list = []
        name = os.path.basename(sf_file).split('.')[0]
        flag_list.append(f"-o audio.jack.id=\"{prefix}{name}\"")
        flag_list.append(f"-o midi.portname=\"{prefix}{name}\"")
        return ' '.join(flag_list)

    def launch(self):
        """ Start a Poppen for each soundfont """
        commands = []
        term_offset = 0
        for sf_file in self.sound_font_list:
            term_offset = term_offset + 30
            offset_string = (f"+{term_offset}+{term_offset}")
            the_command = self.make_command(sf_file, offset_string)
            commands.append(the_command)

        processes = []
        for cmd in commands:
            proc =  subprocess.Popen(cmd, shell=True)
            processes.append(proc)
            time.sleep(1.2)
        for proc in processes:
            proc.wait()
